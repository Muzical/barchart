<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Quote;

class QuotesController extends Controller
{
	public function getAll(){
		return DB::table('quotes')->pluck('symbol');
	}

	public function watch(Request $request){
		$valid = $request->validate(
			['symbol' => ['required','string','exists:quotes,symbol',Rule::notIn(Quote::all()->where('watched', 1)->pluck('symbol')->toArray())]
		    ],
			['exists' => 'The given symbol does not exist.',
			'not_in' => 'This symbol has already been added to the watchlist.']
		);

		$quote = Quote::findOrFail($request->symbol);
		$quote->watched = 1;
		$quote->save();
	}

	public function unwatch(Request $request){
		$valid = $request->validate(
			['symbol' => ['required','string','exists:quotes,symbol',Rule::notIn(Quote::all()->where('watched', 0)->pluck('symbol')->toArray())]
		    ],
			['exists' => 'The given symbol does not exist.',
			 'not_in' => 'This symbol has already been unwatched.']
		);

		$quote = Quote::findOrFail($request->symbol);
		$quote->watched = 0;
		$quote->save();
	}
}
