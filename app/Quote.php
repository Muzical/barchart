<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
	/**
	 * The pre-defined table has a primary key, but Laravel
	 * expects this to be 'id' -- Thankfully, we can
	 * easily override this.
	 *
	 * @var string
	 */
	protected $primaryKey = "symbol";

	/**
	 * As a result of the above, we have to tell Laravel it's a string.
	 *
	 * @var boolean
	 */
	public $incrementing = false;

	/**
	 * @var string
	 */
	protected $keyType = 'string';

	/**
	 * As the user can only change whether or not he is
	 * watching a stock, there is no need to expose
	 * any other database columns to alteration.
	 *
	 * This is obviously not entirely reflective of a
	 * real-world usecase.
	 *
	 * @var array
	 */
	protected $fillable = ['watched'];

	/**
	 * For some reason, 'tradetime' was not being treated as a DateTime in Blade, so this explicitly tells Laravel what to do.
	 *
	 * @var array
	 */
	protected $dates = [
        'created_at',
        'updated_at',
        'tradetime'
    ];
}
