<?php

use Illuminate\Http\Request;

Route::get('getAll', 'QuotesController@getAll');

Route::post('watch', 'QuotesController@watch');

Route::post('unwatch', 'QuotesController@unwatch');
