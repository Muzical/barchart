<?php

use App\Quote;

Route::get('/', function () {
	return view('symbol_display',['symbol_data' => App\Quote::all()->where('watched', 1)->toArray()]);
});
