function validateInput(input, event){
	'use strict';

	event.preventDefault();

	if(input !== null && input !== ""){
		$.ajax({
			method: 'GET',
			cache: false,
			url: '/api/getAll'
		})
		.done(function(data, textStatus, jqXHR){
			if(data !== null && $.isArray(data) && data.indexOf(input.toUpperCase()) === -1){ //The AJAX return is valid but the symbol does not exist.
				$('#jsErrors').html('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><p>The given symbol does not exist.</p>');

				$('#jsErrors').addClass('alert');
				$('#jsErrors').addClass('alert-danger');
				$('#jsErrors').addClass('alert-dismissible');

				$('#jsErrors').removeClass('hidden');
				$('#jsErrors').removeClass('invisible');

				$('#jsErrors').show();

			}else{
				$('#jsErrors').removeClass('alert');
				$('#jsErrors').removeClass('alert-danger');
				$('#jsErrors').removeClass('alert-dismissible');

				$('#jsErrors').addClass('hidden');
				$('#jsErrors').addClass('invisible');

				$('#jsErrors').hide();

				editWatchList(input.toUpperCase(), 'watch'); //Pass it along
			}
		})
		.fail(function(jqXHR, textStatus, errorThrown){
			console.warn(errorThrown); //In the "real world", I would ask my supervisor how to handle this instance.
		});
	}else{
		alert('Please enter a symbol before submitting.');
	}
}

function editWatchList(symbol, method){
	'use strict';

	$.ajax({
		method: 'POST',
		cache: false,
		url: 'api/' + method + '/?symbol=' + symbol
	})
	.done(function(data, textStatus, jqXHR){
		location.reload(true); //true prevents caching
		return true;
	})
	.fail(function(jqXHR, textStatus, errorThrown){
		//There IS one case where the code is "supposed" to fail; we deal with that here.
		if(typeof(jqXHR.responseJSON) !== 'undefined' &&
		   typeof(jqXHR.responseJSON.errors) !== 'undefined' &&
		   $.isArray(jqXHR.responseJSON.errors.symbol) &&
		   typeof(jqXHR.responseJSON.errors.symbol.length) !== 'undefined' &&
		   jqXHR.responseJSON.errors.symbol.length){
			$('#jsErrors').html('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><p>This symbol has already been added to the watchlist.</p>');

			$('#jsErrors').addClass('alert');
			$('#jsErrors').addClass('alert-danger');
			$('#jsErrors').addClass('alert-dismissible');

			$('#jsErrors').removeClass('hidden');
			$('#jsErrors').removeClass('invisible');

			$('#jsErrors').show();
		}else{ //Something's really broken
			console.warn(errorThrown);
		}
	});
}
