# How to set up this project locally

* Since I wrote this on my MacBook Pro, I used Laravel's [Valet](https://laravel.com/docs/5.5/valet) service, v. 2.0.7, to serve my site.  This is the most "batteries included" way to run my code.
* An alternative to this is Laravel's [Homestead](https://laravel.com/docs/5.5/homestead) VM.  This requires [Vagrant](https://www.vagrantup.com/) and either [VirtualBox](https://www.virtualbox.org/) or [VMWare](https://www.vmware.com/), with [Parallels](https://www.parallels.com/) as a third option for Mac.
* If Docker is preferred, [LaraDock](http://laradock.io/) is another option.
* The .env config presumes that the database name is `barchart` and the user is `homestead` with password `secret`.
* Regardless of the environment used, (though it must meet [minimum requirements](https://laravel.com/docs/5.5/#server-requirements),) one must run the following:

1. `composer install` (This step is VITAL!  Composer can be downloaded from [here](https://getcomposer.org/) and requires at least PHP 5.3.2 in order to run.)
1. `npm install` ([Node](http://nodejs.org) is obviously required)
1. `npm run prod`
1. `php artisan migrate`  (This will also serve to indicate if the connection to the database is working.  Please check the .env file if it is not.)
1. `php artisan serve` (If one does not wish to use a pre-made solution such as Homestead or Laradock, this will use the server that is built in to PHP 5.4 onward.  If a server is already running, skip this step.)

## Other notes

* I thought about normalizing the database, but I was unsure as to what degree I was permitted to modify what I was given.  I added some columns, but that is it, as far as the table structure.
