<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>Barchart @yield('title')</title>

	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<link rel="stylesheet" href="css/app.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.css">

	<script src="js/app.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/locale/bootstrap-table-en-US.js"></script>
	<script src="js/barchart.min.js"></script>
</head>
<body>
	<h1 class="container center">Barchart</h1>
	@include('symbol_watch')
	@include('errors')
    @yield('content')
</body>
</html>
