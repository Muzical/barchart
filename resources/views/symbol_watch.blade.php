<div id="jsErrors" class="container hidden invisible show" role="alert"></div>
<div class="container mb-5">
	<form id="add-symbol-form" class="form-inline" accept-charset=utf-8 autocomplete="off">
		{{ csrf_field() }}
		<div class="container">
			<input type="text" class="form-control" name="symbol" id="symbol" placeholder="Enter a symbol..." required>
			<button type="submit" class="btn btn-dark" onClick="validateInput($('#symbol').val(), event);">Add Symbol</button>
		</div>
	</form>
</div>

<script>
	//Enter will have the same effect as clicking the button
	$(document).ready(function(){
		$(document).keypress(function(e) {
			if($('#symbol').val() !== '' && e.which == 13) { //Only submit if the user hit enter AND there is something in the input box to validate.
				validateInput($('#symbol').val());
			}
		});
	});
</script>
