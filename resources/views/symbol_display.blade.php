@extends('master', ['symbol_data' => $symbol_data])

@section('content')
	<div id="content" class="container">
		<div class="row">
			<table id="symbol-table" class="table table-striped table-hover table-sm" data-sort-stable="true" data-toggle="table">
				<thead>
					<th scope="col" data-sortable="true">Symbol</th>
					<th scope="col" data-sortable="true">Symbol Name</th>
					<th scope="col" data-sortable="true">Last Price</th>
					<th scope="col" data-sortable="true">Change</th>
					<th scope="col" data-sortable="true">&percnt;Change</th>
					<th scope="col" data-sortable="true">Volume</th>
					<th scope="col" data-sortable="true">Time</th>
					<th scope="col">&nbsp;</th>
				</thead>
				<tbody>
					@forelse ($symbol_data as $thisSymbol)
						<tr>
							<td scope="row">{{ $thisSymbol['symbol'] }}</td>
							<td>{{ $thisSymbol['name'] }}</td>
							<td>{{ $thisSymbol['last'] }}</td>
							<td>{{ $thisSymbol['change'] }}</td>
							<td>@if($thisSymbol['pctchange'] != 0)
									{{ $thisSymbol['pctchange'] }}&percnt;
							    @else
									unch
								@endif</td>
							<td>{{ $thisSymbol['volume'] }}</td>
							<td>@php $dt = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $thisSymbol['tradetime'],'America/New_York');
							@endphp
							@if($dt->isSameDay(Carbon\Carbon::now()))
								{{ $dt->format('H:i T') }}
							@else
								{{ $dt->format('m/d/y') }}
							@endif</td>
							<td><a style="color:black;" href="#" onClick="editWatchList('{{ $thisSymbol["symbol"] }}', 'unwatch');">&times;</a></td>
						</tr>
					@empty
						<tr>
							<td colspan="8">There are no symbols in your watch list, please add one.</td>
						</tr>
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
@endsection
